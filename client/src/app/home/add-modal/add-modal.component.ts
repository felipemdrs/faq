import { Component, AfterViewInit, OnInit, OnDestroy, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { HomeService } from '../home.service';

import * as $ from 'jquery';

declare var jQuery: any;

@Component({
    selector: 'app-addmodal',
    templateUrl: 'add-modal.template.html',
    providers: [HomeService]
})
export class AddModalComponent implements OnInit {

    newQuestionForm: FormGroup;

    constructor(private el: ElementRef,
                private formBuilder: FormBuilder,
                private homeService: HomeService) {
        this.createForm();
    }

    ngOnInit() {
    }

    newQuestion() {
        if (this.newQuestionForm.valid) {
            this.homeService.create(this.newQuestionForm.value).subscribe(res => {
                this.el.nativeElement
                    .dispatchEvent(new CustomEvent('question-created', {
                        detail: res,
                    }));
                this.newQuestionForm.reset();
                jQuery('#modal-form').modal('hide');
            });
        }
    }

    onKeyDown(event: any) {
        event.preventDefault();
        this.newQuestion();
    }

    private createForm() {
        this.newQuestionForm = this.formBuilder.group({
            title: ['', Validators.required],
            description: ['', Validators.required],
            tags: ''
        });
    }
}