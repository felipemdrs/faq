import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { AddModalComponent } from './add-modal.component';

@NgModule({
    declarations: [AddModalComponent],
    imports     : [
        BrowserModule,
        RouterModule,
        ReactiveFormsModule
    ],
    exports     : [AddModalComponent],
})

export class AddModalModule {}