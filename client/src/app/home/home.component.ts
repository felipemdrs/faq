import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { Logger } from '../core/logger.service';
import { I18nService } from '../core/i18n.service';

import { HomeService } from './home.service';

import * as _ from 'lodash';

const log = new Logger('Login');

@Component({
  selector: 'app-login',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [HomeService]
})
export class HomeComponent implements OnInit {

  version: string = environment.version;
  error: string;
  loginForm: FormGroup;
  isLoading = false;
  questions : any[];

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private i18nService: I18nService,
              private homeService: HomeService) {

  }

  ngOnInit() {
    this.refreshQuestionList();
  }

  refreshQuestionList() {
    this.homeService.getQuestionList(true)
        .subscribe((questionList: any[]) => {
          _.map(questionList, (question: any) => {
            question.tags = question.tags ? question.tags.split(',') : '';
          });

          this.questions = questionList;
    });
  }

  setLanguage(language: string) {
    this.i18nService.language = language;
  }

  get currentLanguage(): string {
    return this.i18nService.language;
  }

  get languages(): string[] {
    return this.i18nService.supportedLanguages;
  }

  removeQuestion(id: Number) {
    this.homeService.removeQuestion(id).subscribe((res: any) => {
      this.refreshQuestionList();
    })
  }

  onQuestionCreated(event: any) {
    let question = event.detail;

    question.tags = question.tags ? question.tags.split(',') : '';
    this.questions.push(question);
  }
}
