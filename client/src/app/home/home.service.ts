import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';

import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';

@Injectable()
export class HomeService {

  questionListData: any;
  questionListChange: Observable<any>;
  questionListChangeObserver: Observer<any>;

  constructor(private http: Http) {
    this.questionListData = null;

    this.questionListChange = new Observable(observer => {
      this.questionListChangeObserver = observer;
    });
  }

  getQuestionList(forced: boolean = false) {
    if (this.questionListData === null || forced) {
      return this.http.get('/questions')
        .map(res => res.json())
        .do((questionList: any) => {
          this.setQuestionList(questionList)
        });
    }

    return Observable.of(this.questionListData);
  }

  setQuestionList(questionList: any) {
    this.questionListData = questionList;

    if (this.questionListChangeObserver) {
      this.questionListChangeObserver.next(this.questionListData);
    }
  }

  create(question: any) {
    return this.http.post('/questions', question)
      .map(res => res.json());
  }

  removeQuestion(id: any) {
    return this.http.delete(`/questions/${id}`);
  }
}